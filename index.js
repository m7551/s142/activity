
console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
let workAddress = {
	houseNumber: "32", 
	street: "Washington",
	city: "Lincon",
	state: "Nebraska" 
}

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies: ");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

function printUserInfo() {
	console.log (firstName + ' ' + lastName + ' is ' + age + ' years of age');
	console.log(hobbies);
	console.log(workAddress);

}

printUserInfo();